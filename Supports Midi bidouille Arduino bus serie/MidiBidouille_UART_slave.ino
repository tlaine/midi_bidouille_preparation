/*
    UART slave program
*/

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);                 // init led pin as an output
  Serial1.begin(9600);                          // init serial communication with "master", at 9600 bauds
}

unsigned long t = 0;                            // time variable
uint8_t intensity = 100;                        // intensity variable
uint8_t on = 100;                               // on time variable
uint8_t off = 100;                              // off time variable

void loop()
{
  //  begin
  unsigned long start = millis();

  //  serial stuff
  if(Serial1.available())                       // if receiving buffer is not empty
  {
    uint8_t command = Serial1.read();           // first byte always correspond to command
    if(command == 0xFF)                         // received command correspond to a "read" command
    {
      Serial1.write(intensity);                 // send intensity
      Serial1.write(on);                        // send on time
      Serial1.write(off);                       // send off time
    }
    else
    {
      delay(10);                                // wait until message fully received
      int value = Serial1.read();               // push received byte int value
      switch(command)                           // change value acording to command byte
      {
        case 'i':  intensity = value;   break;  // change intensity
        case 'o':  on = value;          break;  // change on time
        case 'O':  off = value;         break;  // change off time
        default: break;
      }
    }
  }

  // write led state (off or on with intensity)
  if(t < on)
    analogWrite(LED_BUILTIN, intensity);
  else
    analogWrite(LED_BUILTIN, 0);

  // increment time
  t += millis() - start;
  t %= (on + off);
}
