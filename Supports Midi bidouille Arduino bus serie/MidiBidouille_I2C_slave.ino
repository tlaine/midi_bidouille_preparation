/*
    I²C slave program
*/


#include <Wire.h>

void setup()
{
  Wire.begin(0x05);                // join i2c bus with address 0x05
  Wire.onReceive(receiveEvent);    // write callback
  Wire.onRequest(requestEvent);    // read callback
}

unsigned long t = 0;                            // time variable
volatile uint8_t intensity = 100;               // intensity variable
volatile uint8_t on = 100;                      // on time variable
volatile uint8_t off = 100;                     // off time variable

void loop()
{
  //  begin
  unsigned long start = millis();

  // write led state (off or on with intensity)
  if(t < on)
    analogWrite(LED_BUILTIN, intensity);
  else
    analogWrite(LED_BUILTIN, 0);

  // increment time
  t += millis() - start;
  t %= (on + off);
}


// read callback
void receiveEvent(int howMany)
{
  if(Wire.available()>=2)                         // if a full message is received in buffer
  {
    switch(Wire.read())                           // first byte is always a command byte
    {
      case 'i':  intensity = Wire.read(); break;  // change intensity
      case 'o':  on = Wire.read();        break;  // change on time
      case 'O':  off = Wire.read();       break;  // change off time
      default: break;
    }
  }
}

// write callback
void requestEvent()
{
  // respond with message of 3 bytes, as expected by master
  Wire.write(intensity);                          // send intensity
  Wire.write(on);                                 // send on time
  Wire.write(off);                                // send off time
}
