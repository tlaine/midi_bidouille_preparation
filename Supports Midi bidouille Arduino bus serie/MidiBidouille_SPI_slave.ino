/*
    SPI slave program
*/

#include <SPI.h>

#define CS 3


void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);                 // init led pin as an output
  pinMode(SCK, INPUT);                          // init SCK pin as an input (default state : high)
  pinMode(MOSI, INPUT);                         // init MOSI as an input
  pinMode(MISO, OUTPUT);                        // init MISO as an output
  pinMode(CS, INPUT_PULLUP);                    // init SS an an input
  SPCR = (1 << SPE);                            // enable SPI peripheral (set bit SPE in SPCR register)
}

uint8_t SPItransfer(uint8_t value)
{
  SPDR = value;                                 // prepare SPI data register to send
  while(!(SPSR&(1<<SPIF)) && !digitalRead(CS)); // wait until byte is sent
  asm("nop");                                   // wait one clock tick
  return SPDR;                                  // return the master response byte
}

unsigned long t = 0;                            // time variable
uint8_t intensity = 100;                        // intensity variable
uint8_t on = 100;                               // on time variable
uint8_t off = 100;                              // off time variable
bool SSlast = false;                            // SS pin state variable (init at low)

void loop()
{
  //  begin
  unsigned long start = millis();
  
  //  SPI proceding block
  if (digitalRead(CS) == LOW)                   // Master select slave
  {
      uint8_t command = SPItransfer(0xFF);      // read received byte
      switch(command)
      {
        case 'i': intensity = SPItransfer('i'); break; // change intensity
        case 'o': on = SPItransfer('o');        break; // change on time
        case 'O': off = SPItransfer('O');       break; // change off time
        case 0xFF:
          SPItransfer(intensity);                      // send intensity
          SPItransfer(on);                             // send on time
          SPItransfer(off);                            // send off time
          break;
        default:  SPItransfer(0x00);            break; // send a useless byte
      }
  }

  // write led state (off or on with intensity)
  if(t < on)
    analogWrite(LED_BUILTIN, intensity);
  else
    analogWrite(LED_BUILTIN, 0);

  // increment time
  t += millis() - start;
  t %= (on + off);
}
