/*
    SPI master program
*/

#include <SPI.h>

#define CS 3

void setup()
{ 
  Serial.begin(9600);                                     // init serial communication with computer
  while(!Serial);                                         // wait until the user open a monitor
  Serial.setTimeout(6000);                                // set the monitor timeout to 6 seconds (used for user command entry)
  Serial.println("Hello friend !");                       // print a message

  SPI.begin();                                            // initialize SPI peripheral (as a master)
  pinMode(CS, OUTPUT);                                    // set SS pin as an output
  digitalWrite(CS, HIGH);                                 // set SS to high (slave is selected on low state)
}

//  request a value from user
int askUserFor(String s)
{
  Serial.print(">> enter a ");                            // print a message
  Serial.print(s);
  Serial.println(" please ..");
  while(Serial.available())                               // flush entry buffer
    Serial.read();
      
  String message = Serial.readStringUntil('\n');          // get the user response (finished by '\n' or a timeout)
  int result = message.toInt();                           // parse into int (if it's not an int the function return 0)
  Serial.print("<< '");                                   // print a message
  Serial.print(result);
  Serial.println("'");
  return result;                                          // return the user response as an int
}


void loop()
{
  if(Serial.available())                                  // if a new char was entered by user
  {
    String message = Serial.readStringUntil('\n');        // get the user command (finished by '\n' or a timeout)
    if(message.equalsIgnoreCase("help")) // help command from user
    {
      Serial.println(">> list of available commands :");  // print all available commands
      Serial.println("  intensity");
      Serial.println("  on");
      Serial.println("  off");
      Serial.println("  read");
    }
    else if(message.equalsIgnoreCase("intensity")) // intensity command from user
    {
      uint8_t i = askUserFor("intensity");

      digitalWrite(CS, LOW);                              // select slave
      delayMicroseconds(100);                             // wait synchronization
      SPI.transfer('i');                                  // send command byte
      delayMicroseconds(100);                             // wait for byte sent
      SPI.transfer(i);                                    // send intensity value
      delayMicroseconds(100);                             // wait for byte sent
      digitalWrite(CS, HIGH);                             // unselect slave
    }
    else if(message.equalsIgnoreCase("read"))
    { 
      digitalWrite(CS, LOW);                              // select slave
      delayMicroseconds(100);                             // wait synchronization
      SPI.transfer(0xFF);                                 // sends read command
      delayMicroseconds(100);                             // wait for byte sent

      uint8_t intensity = SPI.transfer(0x00);             // request first byte (intensity)
      delayMicroseconds(100);                             // wait for byte sent
      uint8_t on = SPI.transfer(0x00);                    // request second byte (on time)
      delayMicroseconds(100);                             // wait for byte sent
      uint8_t off = SPI.transfer(0x00);                   // request third byte (off time)
      delayMicroseconds(100);                             // wait for byte sent
      digitalWrite(CS, HIGH);                             // unselect slave
      
      Serial.println("readed :");                         // print message
      Serial.print("  intensity : ");                     
      Serial.println((int)intensity);
      Serial.print("  on time : ");
      Serial.println((int)on);
      Serial.print("  off time : ");
      Serial.println((int)off);
    }
    else // unknown command from user
    {
      Serial.print(">> unknown command:\n\t'");           // print error message
      Serial.print(message);
      Serial.println("'");
      Serial.println(">> enter 'help' for a list of available command");
    }
  }
}
