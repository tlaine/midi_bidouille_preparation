/*
    I²C master program
*/

#include <Wire.h>

void setup()
{
  Serial.begin(9600);                                     // init serial communication with computer
  while(!Serial);                                         // wait until the user open a monitor
  Serial.setTimeout(6000);                                // set the monitor timeout to 6 seconds (used for user command entry)
  Serial.println("Hello friend !");                       // print a message

  Wire.begin();                                           //  init i2c bus (no address for master)
}


int askUserFor(String s)
{
  Serial.print(">> enter a ");                            // print a message
  Serial.print(s);
  Serial.println(" please ..");
  while(Serial.available())                               // flush entry buffer
    Serial.read();
      
  String message = Serial.readStringUntil('\n');          // get the user response (finished by '\n' or a timeout)
  int result = message.toInt();                           // parse into int (if it's not an int the function return 0)
  Serial.print("<< '");                                   // print a message
  Serial.print(result);
  Serial.println("'");
  return result;                                          // return the user response as an int
}


void loop()
{
  if(Serial.available())                                  // if a new char was entered by user
  {
    String message = Serial.readStringUntil('\n');        // get the user command (finished by '\n' or a timeout)
    if(message.equalsIgnoreCase("help")) // help command from user
    {
      Serial.println(">> list of available commands :");  // print all available commands
      Serial.println("  intensity");
      Serial.println("  on");
      Serial.println("  off");
      Serial.println("  read");
    }
    else if(message.equalsIgnoreCase("intensity")) // intensity command from user
    {
      uint8_t i = askUserFor("intensity");                // request intensity from user
      
      Wire.beginTransmission(0x05);                       // transmit to slave 0x05
      Wire.write('i');                                    // sends one byte
      Wire.write(i);                                      // sends one byte
      Wire.endTransmission();                             // stop transmitting
    }
    else if(message.equalsIgnoreCase("read")) // read command from user
    {
      Wire.requestFrom(0x05, 3);                          // request 3 bytes from slave device 0x05
      delay(10);                                          // wait until message fully sent, proceded and for the response
      Serial.println("readed :");                         // print message
      
      uint8_t i = 0;                                      // byte index init
      while(Wire.available())                             // for each byte received
      {
        i++;                                              // increment receptionindex
        int value = Wire.read();                          // receive a byte as character
        switch(i)
        {
          case 1:  Serial.print("  intensity : "); break; // first byte is intensity
          case 2:  Serial.print("  on time : ");   break; // second in on time
          case 3:  Serial.print("  off time : ");  break; // third is off time
          default: Serial.print("  ?? : ");        break; // others are errors
        }
        Serial.println(value);                            // print message
      }
      if(i==0)
        Serial.println("  error when reading, sry :(");   // error : nothing received
    }
    else // unknown command from user
    {
      Serial.print(">> unknown command:\n\t'");           // print error message
      Serial.print(message);
      Serial.println("'");
      Serial.println(">> enter 'help' for a list of available command");
    }
  }
}
